#install.packages("C50")
library("C50")
data <- matrix(c(
  5.2, 5.2, 5.2, 5.2, 5.5, 5.5, 5, 5.5, 6, 5.7,
  2, 3, 2, 4, 2, 4, 3, 3, 3, 3,
  16, 32, 16, 32, 16, 64, 32, 32, 16, 32,
  13, 12, 13, 12, 13, 12, 12, 16, 16, 12.3,
  1049, 2049, 999, 1869, 769, 2799, 1469, 1599, 2199, 1899), 10, 5)
labels <- c("1", "2", "3", "4", "5")
result <- c(3, 3, 4, 4, 3, 4, 4, 3, 4, 4)
test <- factor(result, labels)
colnames(data) <- c("Screen", "RAM", "Memory", "Camera", "Price")
treeModel <- C5.0(x = data[,], y = test, rules = TRUE)
summary(treeModel)
